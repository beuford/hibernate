

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.mapping.List;
import org.quickconnectfamily.json.*;

public class ActiveConnection implements Runnable {

	private Socket socket;

	public ActiveConnection(Socket s) {
		socket = s;
	}

	@Override
	public void run() {
		try {
			JSONInputStream jsonIn = new JSONInputStream(socket.getInputStream());
			JSONOutputStream jsonOut = new JSONOutputStream(socket.getOutputStream());
			HashMap parsedJSONMap = (HashMap) jsonIn.readObject();

			// Following is irrelevant as of the SrvHandler.java
			String serverResponse;
//			ItemDAO
			HashMap items = (HashMap) parsedJSONMap.get("items");
			ItemDAO userdb = new ItemDAO();
			Item newItem = new Item(items.get("item").toString(), items.get("desc").toString());
			serverResponse = "\nItem added: " + userdb.addItem(newItem)+ ", User(" + newItem.getItem()+")";
			serverResponse += "\nItem Auth: " + userdb.register(newItem)+ ", User(" + newItem.getItem()+")";
			jsonOut.writeObject(serverResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}