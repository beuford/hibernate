
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;


@Entity
@Table(name = "user")
public class Item {
    
    private Integer id;
    private String item;
    private String description;
    
    /*
     * one User can have many phone numbers.  CascadeType.ALL causes associated
     * phone numbers to be deleted when a User is deleted.
     */
    
	public Item(String item, String desc) {
        this.item = item;
        this.description = desc;
    }
    
    public Item() {
	}

	public String toString() {
        return "Item [id=" + id + ", item=" + item + ", description=" + description +"]";
    }
    	
	@Id
    @GeneratedValue
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getItem() {
        return item;
    }
    public void setItem(String desc) {
        this.description = desc;
    }
    public String getDesc() {
        return description;
    }
    public void setDesc(String desc) {
        this.description = desc;
    }
}
