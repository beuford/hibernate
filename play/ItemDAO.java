

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ItemDAO {
	public ItemDAO(){
	}
	public boolean addItem(Item item){
		try{
			Session session = HibernateUtilSingleton.getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();		
			session.save(item);
			transaction.commit();
			session.close();
			return true;
		}
		catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean removeItem(Item item){
		try {
			Session session = HibernateUtilSingleton.getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();	
			session.delete(item);
			transaction.commit();
			session.close();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean register(Item item){
		try {
			Session session = HibernateUtilSingleton.getSessionFactory().openSession();
			Query query = session.createQuery("FROM Item WHERE item='" + item.getItem() + "' AND description='" + item.getDesc() + "'");
			return (query.uniqueResult()) != null;
		} catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
	}
	public String showItems(){
		final List<Item> item;
		Session session = HibernateUtilSingleton.getSessionFactory().getCurrentSession();
//        Transaction transaction = session.beginTransaction();
        Query allItemsQuery = session.createQuery("SELECT item from Item");
        item = allItemsQuery.list();
        return "Number of Items: "+item.size();
	}
	
}
