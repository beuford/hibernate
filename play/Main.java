

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

   public static void main(String[] args) throws IOException {
      try 
      {
         ServerSocket server = new ServerSocket(9292);
         System.out.println("Waiting for clients...");
      
         while (true)
         {  Socket s = server.accept();
            
            System.out.println("Client connected from " + s.getLocalAddress().getHostName());
            
            ActiveConnection c = new ActiveConnection(s);
            
            Thread t = new Thread(c);
            t.start();
         }
      } 
      catch (Exception e) 
      {
         System.out.println("An error occured.");
         e.printStackTrace();
      }
   }
}