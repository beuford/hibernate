


import java.util.Date;
import java.util.GregorianCalendar;

import org.hibernate.Transaction;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;


public class TestEmployee {

	public static void main(String[] args) {
		Session session = HibernateUtilSingleton.getSessionFactory().getCurrentSession();
		
		Transaction transaction = session.beginTransaction();
		
		Employee alex = new Employee("Alex Berry", "alexpass", "alex@email.com");

		session.save(alex);
		
		transaction.commit();  // where it actually gets written to database
		
		System.out.println("User generated ID is: " + alex.getEmpId());
	}
}
