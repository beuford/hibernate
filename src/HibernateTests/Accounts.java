package HibernateTests;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Accounts {
	
	private int accountBallance;
	CompoundKey compoundKey;
	
	@Id
	public CompoundKey getCompoundKey() {
		return compoundKey;
	}
	public void setCompoundKey(CompoundKey compundKey) {
		this.compoundKey = compundKey;
	}
	public int getAccountBallance() {
		return accountBallance;
	}
	public void setAccountBallance(int accountBallance) {
		this.accountBallance = accountBallance;
	}
}
