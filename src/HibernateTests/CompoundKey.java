package HibernateTests;
import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class CompoundKey implements Serializable{
	
	private int userID;
	private int accountID;
	
	public CompoundKey(int userID, int accountID){
		this.userID = userID;
		this.accountID = accountID;
	}
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public int getAccountID() {
		return accountID;
	}
	public void setAccountID(int accountID) {
		this.accountID = accountID;
	}
}
