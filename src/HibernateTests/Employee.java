package HibernateTests;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity  // allows for java standards
@Table(name="EmployeeInfo")
public class Employee {
	
	private int empId;
	private String empName;
	
	private String empPassword;

	private String empEmail;
//	private boolean isPermanent;
//	private Calendar empJoin;
//	private Date empLoginTime;
	
	public Employee(String name, String pass, String email){
		this.empName = name;
		this.empPassword = pass;
		this.empEmail = email;
	}
	
	@Id  // makes this primary key
	@TableGenerator(name="empid", table="emp_pk_tb", pkColumnName="empkey", allocationSize=1)
	
	@GeneratedValue(strategy=GenerationType.TABLE, generator="empid")   // auto increments primary key
	@Column(name="EmployeeID")
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}	

	@Transient
	public String getEmpPassword() {
		return empPassword;
	}
	public void setEmpPassword(String empPassword) {
		this.empPassword = empPassword;
	}
	
	@Column(nullable=false, name="email")
	public String getEmpEmail() {
		return empEmail;
	}
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}
	
//	@Basic1
//	public boolean isPermanent() {
//		return isPermanent;
//	}
//	public void setPermanent(boolean isPermanent) {
//		this.isPermanent = isPermanent;
//	}
//	
//	@Temporal(TemporalType.DATE)
//	public Calendar getEmpJoin() {
//		return empJoin;
//	}
//	public void setEmpJoin(Calendar empJoin) {
//		this.empJoin = empJoin;
//	}
//	
//	@Temporal(TemporalType.TIMESTAMP)
//	public Date getEmpLoginTime() {
//		return empLoginTime;
//	}
//	public void setEmpLoginTime(Date empLoginTime) {
//		this.empLoginTime = empLoginTime;
//	}
}
