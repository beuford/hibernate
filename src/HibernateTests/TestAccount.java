package HibernateTests;
import java.util.Date;
import java.util.GregorianCalendar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class TestAccount {
	public static void main(String[] args) {
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(Accounts.class);
		config.configure("hibernate.cfg.xml"); // supposably there is a better
												// way, why i didnt find hte
												// .xml

		new SchemaExport(config).create(true, true);

		SessionFactory factory = config.buildSessionFactory();
		Session session = factory.getCurrentSession();

		session.beginTransaction();

		CompoundKey key1 = new CompoundKey(100,10001);
		Accounts savings = new Accounts();
		savings.setCompoundKey(key1);
		savings.setAccountBallance(8500);
		
		
		session.save(savings);

		session.getTransaction().commit(); // where it actually gets written to
											// database

	}

}
