package HibernateTests;
import java.util.Date;
import java.util.GregorianCalendar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;


public class TestCustomer {


	public static void main(String[] args) {
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(Customer.class);
		config.configure("hibernate.cfg.xml"); //supposably there is a better way, why i didnt find hte .xml
		
		new SchemaExport(config).create(true, true);
		
		
		SessionFactory factory = config.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		session.beginTransaction();
		
		
		Customer alex = new Customer();
		alex.setCustomerName("alex rod");
		alex.setCustomerAddress("123 sesame, id");
		alex.setCreditScore(700);
		alex.setRewardPoints(12000);
		
		session.save(alex);
		
		session.getTransaction().commit();  // where it actually gets written to database
		
	}
}
