package HibernateTests;
import java.util.Date;
import java.util.GregorianCalendar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class TestSchool {

	public static void main(String[] args) {
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(School.class);
		config.configure("hibernate.cfg.xml"); // supposably there is a better
												// way, why i didnt find hte
												// .xml

		new SchemaExport(config).create(true, true);

		SessionFactory factory = config.buildSessionFactory();
		Session session = factory.getCurrentSession();

		session.beginTransaction();

		SchoolDetail annsDetail = new SchoolDetail();
		annsDetail.setPublic(false);
		annsDetail.setSchoolAddress("123 sessame, id");
		annsDetail.setStudentCount(100);
		
		School stanns = new School();
		stanns.setSchoolName("St. Anns School");
		stanns.setSchoolDetail(annsDetail);

		session.save(stanns);

		session.getTransaction().commit(); // where it actually gets written to
											// database

	}

}
