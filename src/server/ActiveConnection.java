package server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.mapping.List;
import org.quickconnectfamily.json.*;

public class ActiveConnection implements Runnable {

	private Socket socket;

	public ActiveConnection(Socket s) {
		socket = s;
	}

	@Override
	public void run() {
		try {
			HashMap<String, MyHandler> serverHandlers = new HashMap<String, MyHandler>();
			LoginSync loginSync = new LoginSync();
			serverHandlers.put("loginSync", loginSync);
			
			JSONInputStream jsonIn = new JSONInputStream(socket.getInputStream());
			JSONOutputStream jsonOut = new JSONOutputStream(socket.getOutputStream());
			HashMap parsedJSONMap = (HashMap) jsonIn.readObject();
			
			MyHandler handler = serverHandlers.get(parsedJSONMap.get("action"));
			String response = handler.handleIt((HashMap) parsedJSONMap.get("params"));
			System.out.println(response);
			jsonOut.writeObject(response);
			
			socket.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}