package server;
import java.util.ArrayList;
import java.util.HashMap;

public class LoginSync extends MyHandler{
	@Override
	String handleIt(HashMap data) {
		try { 
			String serverResponse;
//			UserDAO
			System.out.println("Getting credentials from hashmap...");
			HashMap credentials = (HashMap) data.get("credentials");
			UserDAO userdb = new UserDAO();
			User newUser = new User(credentials.get("username").toString(), credentials.get("password").toString());
			serverResponse = "\nUser added: " + userdb.addUser(newUser)+ ", User(" + newUser.getUname()+")";
//			serverResponse += "\nUser removed: " + userdb.removeUser(newUser)+ ", " + newUser.getUname();
			serverResponse += "\nUser Auth: " + userdb.authenticate(newUser)+ ", User(" + newUser.getUname()+")";
			serverResponse += "\n" + userdb.showUsers();
			
			System.out.println("Preparing Tasks...");
//			TaskDAO
			ArrayList taskList = (ArrayList) data.get("myTaskList");
			TaskDAO taskdb = new TaskDAO();
			serverResponse += "\nTasks added: "+ taskdb.addTask(taskList)+ ", User(" + newUser.getUname()+")";
			return serverResponse;
	}
	catch (Exception e)
      {
         return e.getMessage();
      }
		
	}

}
