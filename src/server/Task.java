package server;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

@Entity
@Table(name = "task")
public class Task implements Serializable {
    @Id
    @GeneratedValue
    private Integer id;
	private String name;
	private String description;
	
	public Task(String name, String description) {
		this.setName(name);
		this.setDescription(description);
	}
	
	public void setTask(String name, String description) {
		this.setName(name);
		this.setDescription(description);
	}
	
	public String toString() {
		return "Name: " + name + ", Description: " + description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Task(HashMap hashMap) { 
		this.name = (String) hashMap.get("name");
		this.description = (String) hashMap.get("description");
	}
	
	
}
