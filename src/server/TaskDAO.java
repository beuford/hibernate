package server;

import java.util.ArrayList;
import java.util.HashMap;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TaskDAO {
	public TaskDAO(){
	}
	public boolean addTask(ArrayList taskList){
		try {
			Session session = HibernateUtilSingleton.getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();	
			for (int i = 0; i < taskList.size(); i++) {
				Task rTask = new Task((HashMap) taskList.get(i));
				session.save(rTask);
			}
			transaction.commit();
			session.close();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean removeTask(ArrayList tasks){
		return true;
	}
	public boolean removeAllTasks(User user){
		return true;
	}
	
}
