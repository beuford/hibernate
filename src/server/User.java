package server;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;
@Entity
@Table(name = "user")
public class User {
    
    @Id
    @GeneratedValue
    private Integer id;
    @NaturalId
    private String username;
    private String password;
    
    /*
     * one User can have many phone numbers.  CascadeType.ALL causes associated
     * phone numbers to be deleted when a User is deleted.
     */
    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(
               name="user_task",
               joinColumns = { @JoinColumn( name="user_id") },
               inverseJoinColumns = @JoinColumn( name="task_id")
               )
    private Set<Task> tasks;

	public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    public User() {
	}

//	public String toString() {
//        return "User [id=" + id + ", username=" + username + ", password=" + password + ", tasks]";
//    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getUname() {
        return username;
    }
    public void setUname(String uname) {
        this.username = uname;
    }
    public String getPword() {
        return password;
    }
    public void setPword(String pword) {
        this.password = pword;
    }
    public Set<Task> getTasks() {
        return tasks;
    }
}
