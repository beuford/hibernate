package server;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserDAO {
	public UserDAO(){
	}
	public boolean addUser(User user){
		try{
			Session session = HibernateUtilSingleton.getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();		
			session.save(user);
			transaction.commit();
			session.close();
			return true;
		}
		catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean removeUser(User user){
		try {
			Session session = HibernateUtilSingleton.getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();	
			session.delete(user);
			transaction.commit();
			session.close();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean authenticate(User user){
		try {
			Session session = HibernateUtilSingleton.getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();	
			Query query = session.createQuery("FROM User WHERE username='" + user.getUname() + "' AND password='" + user.getPword() + "'");
			return (query.uniqueResult()) != null;
		} catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
	}
	public String showUsers(){
		final List<User> users;
		String allUsers = "";
		Session session = HibernateUtilSingleton.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Query allUsersQuery = session.createQuery("SELECT username from User");
        users = allUsersQuery.list();
        allUsers += "Number of Users: "+users.size() + "\n Users: ";
        
        for (int i=0; i < users.size(); i++) {
        	allUsers += users.get(i) + ", ";
//          System.out.println("Number of Tasks: "+element.getTasks().size());
        }
        transaction.commit();
        return allUsers;
	}
	
}
