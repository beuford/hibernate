import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import server.User;
import server.Task;
import server.TaskDAO;


public class TestTask {
			TaskDAO taskdb = new TaskDAO();
			
			Task cleanCar = new Task("clean car", "vacum car before night");
			Task getGroceries = new Task("groceries", "get milk butter");
			Task doDishes = new Task("dishes", "do them");
			
			ArrayList<Task> tasks = new ArrayList<Task>();
//			String[] title = {"clean car", "groceries", "dishes"};
//			String[] desc = {"vacum car before night", "get milk butter", "do them"};
//			for (int i = 0; i < 3; i++){
//				Task temp = new Task(title[i],desc[i]);
//				tasks.add(temp);
//			}
			
			@Test
			public void testAddTask(){
				tasks.add(cleanCar);
				tasks.add(getGroceries);
				tasks.add(doDishes);
				
				Boolean task1 = taskdb.addTask(tasks);
						
				assertEquals("true", task1);
			}
}
