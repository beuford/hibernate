import static org.junit.Assert.*;
import org.junit.Test;
import server.User;

public class TestUser {
	User tyler = new User("tyler", "relyt");
	User steven = new User("steven", "nevets");
	User greg = new User("greg", "gerg");

	@Test
	public void testUser() {
		User tylerUser = new User("tyler", "relyt");
		User stevenUser = new User("steven", "nevets");
		User gregUser = new User("greg", "gerg");

//		What to really test for????   //////////////////////////////////		
		assertEquals("tyler:relyt", tylerUser);
		// assertEquals("tyler", tyler.getUname());
		assertEquals("steven:nevets", stevenUser);
		assertEquals("greg:gerg", gregUser);
	}
	
	@Test
	public void testGetUname(){
		String tylerName = tyler.getUname();
		String stevenName = steven.getUname();
		String gregName = greg.getUname();
		
		assertEquals("tyler", tylerName);
		assertEquals("steven", stevenName);
		assertEquals("greg", gregName);
	}
	
	@Test
	public void testSetUname(){
		tyler.setUname("Tyler");
		steven.setUname("Steven");
		greg.setUname("Greg");
		
//		What to really test for????   //////////////////////////////////
//		assertEquals();
	}
	
	@Test
	public void testGetPword(){
		String tylerPass = tyler.getPword();
		String stevenPass = steven.getPword();
		String gregPass = greg.getPword();
		
		assertEquals("relyt", tylerPass);
		assertEquals("nevets", stevenPass);
		assertEquals("gerg", gregPass);
	}
	
	@Test
	public void testSetPword(){
		tyler.setPword("relyT");
		steven.setPword("nevetS");
		greg.setPword("gerG");

		//		What to really test for????   //////////////////////////////////
//				assertEquals();
		}
}
