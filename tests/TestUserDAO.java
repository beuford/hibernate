import static org.junit.Assert.*;

import org.junit.Test;

import server.User;
import server.UserDAO;



public class TestUserDAO {
	User tyler = new User("tyler", "relyt");
	User steven = new User ("steven", "nevets");
	User greg = new User("greg", "gerg");
	
	@Test
	public void testAddUser(){
		UserDAO userdb = new UserDAO();
		Boolean user1 = userdb.addUser(tyler);
		Boolean user2 = userdb.addUser(steven);
		Boolean user3 = userdb.addUser(greg);
		
		assertEquals("true", user1);
//		assertEquals("tyler", tyler.getUname());
		assertEquals("true", user2);
		assertEquals("true", user3);
	}
	
	@Test
	public void testAuthenticate(){
		UserDAO userdb = new UserDAO();
		
		Boolean user1 = userdb.authenticate(tyler);
		Boolean user2 = userdb.authenticate(steven);
		Boolean user3 = userdb.authenticate(greg);
		
		assertEquals("true", user1);
		assertEquals("true", user2);
		assertEquals("true", user3);
	}
	
	@Test
	public void testShowUsers(){
		UserDAO userdb = new UserDAO();
		
		String users = userdb.showUsers();
		
		String was = "Number of Users: 3\n Users: tyler, steven, greg, ";
		
		assertEquals(was, users);
	}
	
	@Test
	public void testRemoveUser(){
		UserDAO userdb = new UserDAO();
		
		Boolean user1 = userdb.removeUser(tyler);
		Boolean user2 = userdb.removeUser(steven);
		Boolean user3 = userdb.removeUser(greg);

		assertEquals("true", user1);
		assertEquals("true", user2);
		assertEquals("true", user3);
	}
}
